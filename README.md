# PyGMaps

Google Maps for Python using PyQt. <br>
Show Google Maps in Qt using QWebEngineView and Google Maps API.
<br>
The marker can be moved to any location using `window.moveMarker(latitude, longitude)`

Use a new API key for your own usage. [Get your API key here](https://developers.google.com/maps/web/?pli=1)
<br>
<br>

#### Screenshot
![PyGmaps.png](Screenshots/PyGMaps.PNG)

* <b> Requirements: </b> PyQt5
* <b> Programming language: </b> Python 3.6