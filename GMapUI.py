from PyQt5 import QtWidgets, uic
import sys
import os

basePath = "."

uiFilePath = os.path.join('.', "GMap.ui") # If the project is being run directly, then the parent directory is PyGMaps
if os.path.basename(os.getcwd()) != "PyGMaps": # If the project is included in other project, this name should be the parent
    uiFilePath = os.path.join('PyGMaps', "GMap.ui")

mapUiForm = uic.loadUiType(uiFilePath)[0]

class GMapWindow(QtWidgets.QMainWindow, mapUiForm):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        ## Load html as content strings and then display. To avoid security error "Not allowed to load local resource: file:///...."
        htmlFilePath = os.path.join('.', "GMaps.html")
        if os.path.basename(os.getcwd()) != "PyGMaps":
            htmlFilePath = os.path.join('PyGMaps', "GMaps.html")
        html = open(htmlFilePath).read()
        #print(html)
        self.mapView.setHtml(html)
        #self.mapView.load(QtCore.QUrl.fromLocalFile("/GMaps.html"))
        self.buttonGo.clicked.connect(self.gotoLocation)
        self.i = True

    def moveMarker(self, latitude=12.971599, longitude=77.594563):
        self.mapView.page().runJavaScript('moveMarker({}, {});'.format(latitude, longitude))

    def gotoLocation(self):
        latitude = self.editLatitude.text()
        longitude = self.editLongitude.text()
        self.moveMarker(latitude, longitude)

    def onclick(self):
        print('asdfasdf')
        if self.i:
            self.moveMarker()
        else:
            self.moveMarker(-25.363, 131.044)
        self.i = not self.i

if __name__=="__main__":
    app = QtWidgets.QApplication(sys.argv)
    mapWindow = GMapWindow()
    mapWindow.show()
    sys.exit(app.exec_())


'''app = QtWidgets.QApplication(sys.argv)

w = QtWidgets.QMainWindow()
w.resize(720, 400)
web = QWebEngineView(w)
w.setCentralWidget(web)
button = QtWidgets.QPushButton("asjkdf", w)
web.load(QtCore.QUrl.fromLocalFile("/GMaps.html"))
web.show()
w.setWindowTitle('Simple')
w.show()

i = True
def onclick():
button.resize(200, 200)
button.clicked.connect(onclick)
app.exec_()
import time
time.sleep(2)'''
